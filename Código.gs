var idiomas = ["English","Spanish","German","French","Portuguese"];
    
var i8colunas = { 
       English    : ["Name","Email","Phone"],
       Espanish   : ["Nombre","Correo Eletrónico","Teléfono"],
       German     : ["Name", "Email", "Telefon"],
       French     : ["Nom", "Email", "Téléphone"],
       Portuguese : ["Nome", "E-mail", "Telefone"]
    };

var nomeGrupo = "Telefones";
var planilha = SpreadsheetApp.getActiveSpreadsheet();
var folha = planilha.getSheets()[0];
var rotular = folha.getRange(1,1,1,3).setValues([["Nome","Emails","Telefones"]]);
var grupo = ContactsApp.getContactGroup(nomeGrupo);
if (!grupo) {
    var grupo = ContactsApp.createContactGroup(nomeGrupo);
}

function SyncPhoneGroup() {
  var contatos = ContactsApp.getContacts();
  var linha = 2;
  for (var i=0; i<contatos.length; i++) {
     var telefones = contatos[i].getPhones();
     if (telefones.length) {
        var nome = contatos[i].getFullName();
        var emails = contatos[i].getEmails().toString();
        var gravarEm = folha.getRange(linha,1,1,3).setValues([[nome,emails,telefones[0].getPhoneNumber()]]);
        linha = linha+1; 
     }        
  }
}

function Iniciar() {
  

}

function aoInstalar() {
    aoAbrir();
}

function aoAbrir() {
     SpreadsheetApp.getUi()
     .createAddonMenu()
     .addItem("Start", "Iniciar")
     .addSeparator()
     .addItem("Configure", "Configurar")
     .addToUi();
}